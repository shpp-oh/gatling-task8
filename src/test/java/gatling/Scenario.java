package gatling;

import io.gatling.javaapi.core.CoreDsl;
import io.gatling.javaapi.core.ScenarioBuilder;

public class Scenario {
    public static ScenarioBuilder postScenario = CoreDsl.scenario("post scenario").exec(
            Step.postRequest
    );

    public static ScenarioBuilder putScenario = CoreDsl.scenario("put scenario").exec(
            Step.putRequest
    );

    public static ScenarioBuilder getOneScenario = CoreDsl.scenario("get one scenario").exec(
            Step.getOneRequest
    );

    public static ScenarioBuilder getAllScenario = CoreDsl.scenario("get all scenario").exec(
            Step.getAllRequest
    );

    public static ScenarioBuilder deleteOneScenario = CoreDsl.scenario("delete one scenario").exec(
            Step.deleteOneRequest
    );
}
